# Test task

## Lets image there is no video players. You are the last React engineer and in order to save the world you will need to create a video player.

What this video player need?

1. Button to play video
2. Input to upload video so it could be played
3. Button to stop playing video
4. Video on the screen

## Evaluation criteria

- State management approach
- Code readability
- Testing approach
- Component hierarchy and structure
